//
//  ViewController.h
//  StackOverflowClient
//
//  Created by Wendy Yang on 17/02/2016.
//  Copyright © 2016 Wendy Yang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController <NSURLSessionDataDelegate> // Link our existing view controller to this scene, conforming to the NSURLSessionDataDelegate protocol.


@end


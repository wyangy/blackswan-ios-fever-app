//
//  MasterViewController.h
//  NSHipsterRSS
//
//  Created by Wendy Yang on 21/02/2016.
//  Copyright © 2016 Wendy Yang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : UITableViewController <NSXMLParserDelegate>

@end


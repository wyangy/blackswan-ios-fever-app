//
//  ViewController.m
//  EmergencyCats
//
//  Created by Wendy Yang on 20/02/2016.
//  Copyright © 2016 Wendy Yang. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) NSURLSession *session; // Declare NSURLSession
@property (nonatomic, strong) NSXMLParser *xmlParser;
@property (strong, nonatomic) NSMutableString *urlString; // When we start parsing the xml file, it is parsed one character at a time so we need a temp string to store relevant concurrent characters.
@property (strong, nonatomic) NSMutableArray *imagesArray; // Declare array to hold urls of image we want to display
@property (strong, nonatomic) UIActivityIndicatorView *myActivityView; // Responsible for displaying our indicators
@property (strong, nonatomic) NSMutableArray *tasksArray; // Use this array to keep track of our download tasks and we'll create a download task for each piece of artwork.
@property (strong, nonatomic) NSMutableArray *savedImages; // Declare NSArray to store the images in.


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Instantiate our activity view.
    [self initSpinner];
    self.imagesArray = [[NSMutableArray alloc] initWithCapacity:0];
    [self.refreshControl addTarget:self action:@selector(fetchXml) forControlEvents:UIControlEventValueChanged];
    [self fetchXml];
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([self.imagesArray count] != [self.savedImages count]) {
        self.savedImages = [[NSMutableArray alloc] initWithCapacity:0];
        for (int i = 0; i < [self.imagesArray count]; i++) {
            [self.savedImages addObject:[NSNull null]];
        }
    }
    return [self.imagesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.imageView.image = nil;
    
    // Display the images in the cell.
    if (self.savedImages[indexPath.row] != [NSNull null]) {
        [[cell imageView] setImage:self.savedImages[indexPath.row]];
    }
    return cell;
}

- (void)fetchXml {
    [self.imagesArray removeAllObjects];
    [[self tableView] reloadData];
    
    // Instantiate NSURLSession. By specifying "sharedSession", we get the currently set global settings.
    self.session = [NSURLSession sharedSession];
    
    // Declare a NSURL of the asset we wish to download.
    NSURL *myURL = [NSURL URLWithString:@"http://thecatapi.com/api/images/get?format=xml&results_per_page=3"];
    
    // Instantiate "NSURLSessionDataTask" object. This class is responsible for returning the data back to the app.
    NSURLSessionDataTask *dataTask = [self.session dataTaskWithURL:myURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        // Make sure that there is data.
        if (data != nil) {
            self.xmlParser = [[NSXMLParser alloc] initWithData:data];
            self.xmlParser.delegate = self;
            
            // Start parsing.
            [self.xmlParser parse];
        }
        
        // Turn on the network activity view controller as we start the downloaded.
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        // We print the contents of the dictionary to the console and call the block to reload the table.
        dispatch_async(dispatch_get_main_queue(), ^{[self.tableView reloadData];}); // We delcare a block that reloads the table contents when called.
        
        // Start animating the spinner. We are using dispatch_async to make sure we start the spinner on the main thread which handles the UI.
        dispatch_async(dispatch_get_main_queue(), ^{[self.myActivityView startAnimating];});
        
        [self fetchImages];
    }];
    [dataTask resume];
}


#pragma mark - NSXMLParserDelegate method implementation

-(void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
    NSLog(@"%@", [parseError localizedDescription]);
}


-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    // If the current element name is equal to "url" then initialize urlString array.
    if ([elementName isEqualToString:@"url"]) {
        NSLog(@"start found URL");
        self.urlString = [[NSMutableString alloc] init];
    }
}

// Once inside an element, the parser will read the string data and pass it to its delegate through the message "parser:foundCharacters". This method gets called multiple times for a single title and each time we capture the characters found.
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [self.urlString appendString:string];
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    if ([elementName isEqual:@"url"] && ![self.urlString isEqual:@"url"]) {
        NSLog(@"end found url %@", self.urlString);
        [self.imagesArray addObject:[NSString stringWithString:self.urlString]];
        self.urlString = nil;
    }
}

// This method creates a download task for each image.
- (void)fetchImages {
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.tasksArray = [[NSMutableArray alloc] init];
    sessionConfig.HTTPMaximumConnectionsPerHost = 3;
    sessionConfig.timeoutIntervalForResource = 120;
    sessionConfig.timeoutIntervalForRequest = 120;
    self.session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
    
    for (int i = 0; i < [self.imagesArray count]; i++) {
        NSString *myImage = self.imagesArray[i];
        
        NSURLSessionDownloadTask *sessionDownloadTask = [self.session downloadTaskWithURL:[NSURL URLWithString:myImage]];
        [self.tasksArray addObject:sessionDownloadTask];
        [sessionDownloadTask resume];
    }
}

// Add the delegate methods required to capture the downloads. This method gets called when a task has completed.
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    if (!error) {
        NSLog(@"task %lu: finished", (long)[self.tasksArray indexOfObject:task]);
    }
    else if (error.code == NSURLErrorTimedOut) {
        NSLog(@"task %lu: timed out!", (long)[self.tasksArray indexOfObject:task]);
    }
    // Each time a task completes, we call our method that checks if there are any remaining tasks left.
    [self checkForRemainingTasks];
}

// This delegate method called when a download task has finished downloading.
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    NSLog(@"task %lu: downloaded!", (long)[self.tasksArray indexOfObject:downloadTask]);
    NSUInteger taskID = [self.tasksArray indexOfObject:downloadTask];
    
    self.savedImages[taskID] = [UIImage imageWithData:[NSData dataWithContentsOfURL:location]];
    
    dispatch_async(dispatch_get_main_queue(), ^{[self.tableView reloadData];});
}

// We need to tell refreshControl when to stop spinning its indicator.
- (void)checkForRemainingTasks {
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        NSLog(@"Outstanding tasks are %lu", (long)[downloadTasks count]);
        if ([downloadTasks count] == 0) {
            // Next, we need to turn off the network activity with all the downloads have completed. We will use the same method as we used for the refresh control.
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            // Stop the spinner at appropriate times.
            dispatch_async(dispatch_get_main_queue(), ^{[self.myActivityView stopAnimating];});
            dispatch_async(dispatch_get_main_queue(), ^{[self.refreshControl endRefreshing];});
        }
    }];
}

// The "initSpinner" method instantiates a new instance of UIActivityIndicatorView with the greay style spinner.
- (void)initSpinner {
    self.myActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    CGPoint newCenter = [self.view center];
    self.myActivityView.center = newCenter;
    [self.view addSubview:self.myActivityView];
}

@end
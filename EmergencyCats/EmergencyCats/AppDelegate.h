//
//  AppDelegate.h
//  EmergencyCats
//
//  Created by Wendy Yang on 21/02/2016.
//  Copyright © 2016 Wendy Yang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


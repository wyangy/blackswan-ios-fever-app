//
//  ViewController.h
//  EmergencyCats
//
//  Created by Wendy Yang on 20/02/2016.
//  Copyright © 2016 Wendy Yang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UITableViewController <NSXMLParserDelegate, NSURLSessionDataDelegate, UITableViewDelegate, UITableViewDataSource>

@end


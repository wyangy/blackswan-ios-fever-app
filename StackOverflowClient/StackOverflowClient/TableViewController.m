//
//  ViewController.m
//  StackOverflowClient
//
//  Created by Wendy Yang on 17/02/2016.
//  Copyright © 2016 Wendy Yang. All rights reserved.
//

#import "TableViewController.h"

#import "TableViewController.h"

@interface TableViewController ()
@property (strong, nonatomic) NSDictionary *jsonFeed; // Holds entire json feed from Apple.
@property (strong, nonatomic) NSMutableArray *questionFeed; // Stores the specific library.
@property (strong, nonatomic) NSURLSession *session;
@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self fetchJsonFeed];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.questionFeed count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    UILabel *titleLabel = (UILabel *) [cell viewWithTag:1];
    titleLabel.text = self.questionFeed[indexPath.row][@"title"];
    
    NSNumber *score = [[self.questionFeed objectAtIndex:indexPath.row] valueForKey:@"score"];
    UILabel *scoreLabel = (UILabel *) [cell viewWithTag:2];
    scoreLabel.text = [NSString stringWithFormat:@"%@", score];
    
    NSNumber *answer_count = [[self.questionFeed objectAtIndex:indexPath.row] valueForKey:@"answer_count"];
    UILabel *answer_countLabel = (UILabel *) [cell viewWithTag:3];
    answer_countLabel.text = [NSString stringWithFormat:@"%@", answer_count];
    
    NSNumber *view_count = [[self.questionFeed objectAtIndex:indexPath.row] valueForKey:@"view_count"];
    UILabel *view_countLabel = (UILabel *) [cell viewWithTag:4];
    view_countLabel.text = [NSString stringWithFormat:@"%@", view_count];
    
    UILabel *tagLabel = (UILabel *) [cell viewWithTag:5];
    tagLabel.text = self.questionFeed[indexPath.row][@"tags"][0];
    
    return cell;
}

- (void)fetchJsonFeed {
    self.jsonFeed = nil;
    self.questionFeed = nil;
    
    self.session = [NSURLSession sharedSession];
    
    NSURL *myURL = [NSURL URLWithString:@"https://api.stackexchange.com/2.2/questions?page=1&pagesize=5&order=desc&sort=hot&site=stackoverflow"];
    
    NSURLSessionDataTask *dataTask = [self.session dataTaskWithURL:myURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        self.jsonFeed = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

        self.questionFeed = [self.jsonFeed objectForKey:@"items"];

        dispatch_async(dispatch_get_main_queue(), ^{[self.tableView reloadData];});
    }];
    [dataTask resume];
}

@end

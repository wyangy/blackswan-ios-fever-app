//
//  main.m
//  EmergencyCats
//
//  Created by Wendy Yang on 21/02/2016.
//  Copyright © 2016 Wendy Yang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
